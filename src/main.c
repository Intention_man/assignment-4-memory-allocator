// Обычное успешное выделение памяти.
// Освобождение одного блока из нескольких выделенных.
// Освобождение двух блоков из нескольких выделенных.
// Память закончилась, новый регион памяти расширяет старый.
// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.

// Тесты должны запускаться из main.c, но могут быть описаны в отдельном (отдельных) файлах.
// Алгоритм не самый простой, легко ошибиться. Чтобы не тратить времени на отладку, обязательно делайте разбиение на маленькие функции!

#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096
#define HEAP_START ((void*)0x04040000)


static void init_and_check_heap(){
    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);
}

static struct block_header* get_header(void* pointer) {
    return (struct block_header*)((uint8_t*)pointer - offsetof(struct block_header, contents));
}

static void test_allocation(){
    printf("1. test_allocation...\n");

    init_and_check_heap();
    
    void* allocated_block = _malloc(30);
    assert(allocated_block != NULL);

    _free(allocated_block);
    heap_term();
    printf("OK \n\n");
}

static void test_free_one_block(){
    printf("2. test_free_one_block...\n");

    init_and_check_heap();

    void* block1 = _malloc(50);
    void* block2 = _malloc(70);

    _free(block1);
    assert(get_header(block1)->is_free);
    assert(!get_header(block2)->is_free);

    _free(block2);
    heap_term();
    printf("OK \n\n");
}

static void test_free_two_blocks(){
    printf("3. test_free_two_blocks...\n");

    init_and_check_heap();

    void* block1 = _malloc(50);
    void* block2 = _malloc(70);
    void* block3 = _malloc(100);
    void* block4 = _malloc(30);

    _free(block1);
    _free(block3);
    assert(get_header(block1)->is_free);
    assert(!get_header(block2)->is_free);
    assert(get_header(block3)->is_free);
    assert(!get_header(block4)->is_free);

    _free(block2);
    _free(block4);
    heap_term();
    printf("OK \n\n");
}

static void test_extends_region_with_new_one(){ 
    printf("4. test_extends_region_with_new_one...\n");

    void* heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    size_t start_region_size = ((struct region*)heap)->size;

    void* block = _malloc(5 * HEAP_SIZE);
    struct block_header* new_block_header = get_header(block);
    size_t expanded_region_size = ((struct region*)heap)->size;

    assert(start_region_size < expanded_region_size);
    assert(new_block_header);

    _free(block);
    heap_term(); 
    printf("OK \n\n");
}

static void test_allocate_region_in_new_place(){
    printf("5. test_allocate_region_in_new_place...\n");

    init_and_check_heap();

    void* start_region = mmap(HEAP_START + REGION_MIN_SIZE, 200, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

    assert(start_region);

    void* new_block = _malloc(200);
    assert(new_block);
    assert(start_region != new_block);

    heap_term();
    printf("OK \n\n");
}


int main(){
    test_allocation();
    test_free_one_block();
    test_free_two_blocks();
    test_extends_region_with_new_one();
    test_allocate_region_in_new_place();

    printf("All tests passed!");
    return 0;
}